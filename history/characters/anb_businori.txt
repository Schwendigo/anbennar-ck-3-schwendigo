﻿# House Taefdares

taefdares_0001 = {
	name = "Halfdal"
	dynasty = dynasty_divenscourge
	culture = dalric
	religion = skaldhyrric_faith
	
	trait = race_human
	trait = education_martial_3
	trait = greedy
	trait = ambitious
	trait = deceitful
	trait = reaver
	trait = strategist
	trait = avaricious
	trait = viking
	
	father = pearlman_0001
	mother = pearlman_0001_1
	
	867.9.13 = {
		birth = yes
	}
	890.8.25 = {
		add_spouse = luminares_0003
	}
		897.6.10 = {
		add_spouse = urzhana_0001
	}
	910.6.6 = {
		give_nickname = nick_hoarder
	}
	910.10.15 = {
		add_spouse = azaraha_0001
	}
	930.7.16 = {
		death = {
			death_reason = death_battle
		}
	}
}

taefdares_0002 = {
	name = "Henrik"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_diplomacy_3
	trait = just
	trait = diligent
	trait = cynical
	trait = reaver
	
	father = taefdares_0001
	mother = luminares_0003
	
	890.12.18 = {
		birth = yes
	}
	908.8.25 = {
		add_spouse = tefori_0001
	}
	955.1.3 = {
		death = yes
	}
}

taefdares_0003 = {
	name = "Halla"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = patient
	trait = temperate
	trait = lazy
	
	father = taefdares_0001
	mother = luminares_0003
	
	894.7.2 = {
		birth = yes
	}
	940.12.20 = {
		death = yes
	}
}

taefdares_0004 = {
	name = "Rimana"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	father = taefdares_0002
	mother = tefori_0001
	
	trait = race_human
	trait = education_diplomacy_1
	trait = stubborn
	trait = calm
	trait = lazy
	trait = physique_bad_2
	
	910.7.2 = {
		birth = yes
	}
	934.3.16 = {
		add_spouse = orstengard_0004
	}
	944.2.9 = {
		death = yes
	}
}

taefdares_0005 = {
	name = "Halfdal"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = greedy
	trait = fickle
	trait = reaver
	trait = strong
	
	father = taefdares_0002
	mother = tefori_0001
	
	907.8.4 = {
		birth = yes
	}
	926.3.17 = {
		add_spouse = pearlman_0016
	}
	958.11.5 = {
		death = {
			death_reason = death_battle
		}
	}
}

taefdares_0006 = {
	name = "Alfrido"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_learning_3
	trait = zealous
	trait = temperate
	trait = chaste
	trait = reclusive
	
	father = taefdares_0005
	mother = pearlman_0016
	
	931.9.12 = {
		birth = yes
	}
	952.10.11 = {
		add_spouse = crodamos_0006
	}
	982.9.12 = {
		death = yes
	}
}

taefdares_0007 = {
	name = "Halfdal"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_2
	trait = patient
	trait = brave
	trait = vengeful
	trait = reaver
	trait = scarred
	trait = forder
	
	father = taefdares_0006
	mother = crodamos_0006
	
	953.7.11 = {
		birth = yes
	}
	972.9.21 = {
		add_spouse = khasani_0001
	}
	1018.11.6 = {
		death = yes
	}
}

taefdares_0008 = {
	name = "LaI_n"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = rowdy
	trait = brave
	
	father = taefdares_0007
	mother = khasani_0001
	
	974.3.15 = {
		birth = yes
	}
	987.5.4 = {
		death = yes
	}
}

taefdares_0009 = {
	name = "Clara"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_martial_2
	trait = impatient
	trait = temperate
	trait = diligent
	trait = beauty_good_2
	
	father = taefdares_0007
	mother = khasani_0001
	
	989.10.26 = {
		birth = yes
	}
	1009.6.13 = {
		add_spouse = 17
	}
}

#Traitor Taefdares

taefdares_0010 = {
	name = "Oskar"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_3
	trait = wrathful
	trait = impatient
	trait = chaste
	trait = reaver
	trait = forder
	
	father = taefdares_0005
	mother = pearlman_0016
	
	937.9.19 = {
		birth = yes
	}
	960.3.16 = {
		add_spouse = orstengard_0001
	}
	976.9.26 = {
		death = yes
	}
}

taefdares_0011 = {
	name = "Ragnvald"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_intrigue_4
	trait = deceitful
	trait = ambitious
	trait = brave
	trait = disloyal
	trait = intellect_good_2
	
	father = taefdares_0010
	mother = orstengard_0001
	
	966.8.26 = {
		birth = yes
	}
	988.8.26 = {
		add_spouse = tefori_0002
	}
	1007.10.3 = {
		death = {
		death_reason = death_battle
		}
	}
}

taefdares_0012 = {
	name = "Oskar"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_2
	trait = stubborn
	trait = chaste
	trait = brave
	
	father = taefdares_0011
	mother = tefori_0002
	
	990.7.25 = {
		birth = yes
	}
	1007.10.11 = {
		death = {
		death_reason = death_ripped_apart_limb_by_limb
		}
	}
}

taefdares_0013 = {
	name = "Rimana"
	dynasty_house = house_taefdares
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_learning_3
	trait = lazy
	trait = lustful
	trait = gregarious
	
	father = taefdares_0011
	mother = tefori_0002
	
	993.6.3 = {
		birth = yes
	}
	1015.2.16 = {
		add_spouse = luminares_0011
	}
}

# Silebor Dynasty

silebor_0001 = {
	name = "Ariana"
	dynasty = dynasty_silebor
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_half_elf
	trait = charming
	trait = beauty_good_2
	
	father = 17
	mother = taefdares_0009
	
	1010.4.19 = {
		birth = yes
	}
}

silebor_0002 = {
	name = "Borian"
	dynasty = dynasty_silebor
	culture = tefori
	religion = tefori_damish
	
	trait = race_half_elf
	trait = bossy
	trait = beauty_good_2
	
	father = 17
	mother = taefdares_0009
	
	1014.9.28 = {
		birth = yes
	}
}

silebor_0003 = {
	name = "Delian"
	dynasty = dynasty_silebor
	culture = tefori
	religion = tefori_damish

	trait = race_half_elf
	
	father = 17
	mother = taefdares_0009
	
	1017.11.16 = {
		birth = yes
	}
}

# House Luminares

luminares_0001 = {
	name = "Aldrisius"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	
	trait = race_human
	trait = education_diplomacy_3
	trait = gregarious
	trait = trusting
	trait = chaste
	
	831.10.4 = {
		birth = yes
	}
	856.3.11 = {
		add_spouse = waasheshi_0001
	}
	887.3.25 = {
		death = yes
	}
}

luminares_0002 = {
	name = "Dominico"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	
	trait = race_human
	trait = education_diplomacy_2
	trait = fickle
	trait = ambitious
	trait = cynical
	
	father = luminares_0001
	mother = waasheshi_0001
	
	860.1.16 = {
		birth = yes
	}
	895.7.5 = {
		add_spouse = tefori_0003
	}
	930.10.4 = {
		death = yes
	}
}

luminares_0003 = {
	name = "Lisbana"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = wrathful
	trait = shy
	trait = compassionate
	trait = beauty_good_2
	
	father = luminares_0001
	mother = waasheshi_0001
	
	871.3.19 = {
		birth = yes
	}
	890.8.25 = {
		add_spouse = taefdares_0001
	}
	935.8.1 = {
		death = yes
	}
}

luminares_0004 = {
	name = "ArturiA_n"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_3
	trait = fickle
	trait = brave
	trait = vengeful
	
	father = luminares_0002
	mother = tefori_0003
	
	902.10.3 = {
		birth = yes
	}
	928.3.16 = {
		add_spouse = tefori_0004
	}
	957.5.11 = {
		death = yes
	}
}

luminares_0005 = {
	name = "AldrE_s"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	
	trait = race_human
	trait = education_stewardship_3
	trait = callous
	trait = patient
	trait = content
	
	father = luminares_0004
	mother = tefori_0004
	
	932.6.14 = {
		birth = yes
	}
	950.2.11 = {
		add_spouse = tefori_0005
	}
	991.9.28 = {
		death = yes
	}
}

luminares_0006 = {
	name = "Sibila"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_intrigue_3
	trait = vengeful
	trait = lazy
	trait = patient
	
	father = luminares_0004
	mother = tefori_0004
	
	940.11.5 = {
		birth = yes
	}
	1010.1.16 = {
		death = yes
	}
}

luminares_0007 = {
	name = "Celia"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_stewardship_3
	trait = calm
	trait = shy
	trait = eccentric
	trait = journaller
	
	father = luminares_0005
	mother = tefori_0005
	
	950.11.4 = {
		birth = yes
	}
	969.3.28 = {
		add_spouse = orstengard_0005
	}
	1005.4.19 = {
		death = yes
	}
}

luminares_0008 = {
	name = "Emilia"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_learning_3
	trait = chaste
	trait = shy
	trait = eccentric
	trait = reclusive
	
	father = luminares_0005
	mother = tefori_0005
	
	959.1.27 = {
		birth = yes
	}
	1009.10.12 = {
		death = yes
	}
}

luminares_0009 = {
	name = "AucA_n"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	
	trait = race_human
	trait = education_stewardship_2
	trait = humble
	trait = deceitful
	trait = fickle
	
	father = luminares_0005
	mother = tefori_0005
	
	965.2.28 = {
		birth = yes
	}
	987.7.21 = {
		add_spouse = tefori_0006
	}
	1020.6.7 = {
		death = yes
	}
}

luminares_0010 = {
	name = "GiC_ela"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = humble
	trait = lazy
	trait = gregarious
	
	father = luminares_0009
	mother = tefori_0006
	
	990.5.2 = {
		birth = yes
	}
	1009.1.8 = {
		add_spouse = nirhaunes_0005
	}
}

luminares_0011 = {
	name = "ArturiA_n"
	dynasty = dynasty_luminares
	culture = tefori
	religion = tefori_damish
	
	trait = race_human
	trait = education_intrigue_2
	trait = stubborn
	trait = deceitful
	trait = temperate
	
	father = luminares_0009
	mother = tefori_0006
	
	998.5.17 = {
		birth = yes
	}
	1015.2.16 = {
		add_spouse = taefdares_0013
	}
}

# Av Orstengard Dynaty

orstengard_0001 = {
	name = "Iona"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = temperate
	trait = chaste
	trait = wrathful
	
	father = orstengard_0004
	mother = taefdares_0004
	
	940.5.8 = {
		birth = yes
	}
	960.3.16 = {
		add_spouse = taefdares_0010
	}
	1002.2.25 = {
		death = yes
	}
}

orstengard_0002 = {
	name = "Estrid"
	dynasty = dynasty_orstengard
	culture = olavish
	religion = skaldhyrric_faith
	female = yes
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = stubborn
	trait = ambitious
	trait = reaver
	trait = beauty_good_1
	trait = shieldmaiden
	
	881.8.14 = {
		birth = yes
	}
	909.8.15 = {
		add_spouse = orstengard_0003
	}
	943.12.28 = {
		death = yes
	}
}

orstengard_0003 = {
	name = "S_amarkos"
	dynasty = dynasty_visunid
	culture = korbarid
	religion = korbarid_dragon_cult
	
	trait = race_human
	trait = education_intrigue_2
	trait = gregarious
	trait = lustful
	trait = just
	trait = beauty_good_2
	
	890.6.3 = {
		birth = yes
	}
	909.8.15 = {
		add_spouse = orstengard_0002
	}
	935.12.4 = {
		death = yes
	}
}

orstengard_0004 = {
	name = "Ragnvald"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = skaldhyrric_faith
	
	trait = race_human
	trait = education_stewardship_3
	trait = just
	trait = stubborn
	trait = patient
	trait = reaver
	
	father = orstengard_0003
	mother = orstengard_0002
	
	912.2.2 = {
		birth = yes
	}
	934.3.16 = {
		add_spouse = taefdares_0004
	}
	961.10.8 = {
		death = yes
	}
}

orstengard_0005 = {
	name = "Patrik"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_diplomacy_3
	trait = honest
	trait = arrogant
	trait = patient
	trait = reaver
	
	father = orstengard_0004
	mother = taefdares_0004
	
	944.9.13 = {
		birth = yes
	}
	969.3.28 = {
		add_spouse = luminares_0007
	}
	994.3.6 = {
		death = yes
	}
}

orstengard_0006 = {
	name = "Thamur"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_stewardship_2
	trait = greedy
	trait = ambitious
	trait = eccentric
	trait = reaver
	
	father = orstengard_0005
	mother = luminares_0007
	
	970.4.8 = {
		birth = yes
	}
	993.12.2 = {
		add_spouse = nirhaunes_0001
	}
	1007.9.2 = {
		death = yes
	}
}

orstengard_0007 = {
	name = "Estrid"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = tefori_damish
	female = yes
	dna = estrid_orstengard
	
	trait = race_human
	trait = education_martial_2
	trait = greedy
	trait = arrogant
	trait = gregarious
	trait = reaver
	trait = shieldmaiden
	
	father = orstengard_0006
	mother = nirhaunes_0001
	
	994.12.4 = {
		birth = yes
	}
}

orstengard_0008 = {
	name = "Asfrid"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_intrigue_3
	trait = deceitful
	trait = brave
	trait = chaste
	
	father = orstengard_0006
	mother = nirhaunes_0001
	
	998.2.16 = {
		birth = yes
	}
}

orstengard_0009 = {
	name = "Henrik"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = honest
	trait = arbitrary
	trait = reaver
	trait = intellect_good_1
	
	father = orstengard_0005
	mother = luminares_0007
	
	975.11.4 = {
		birth = yes
	}
	999.5.17 = {
		add_spouse = tefori_0007
	}
}

orstengard_0010 = {
	name = "Raubyn"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = vengeful
	trait = stubborn
	trait = beauty_good_1
	trait = intellect_good_1
	
	father = orstengard_0009
	mother = tefori_0007
	
	1002.4.2 = {
		birth = yes
	}
}

orstengard_0011 = {
	name = "Varmir"
	dynasty = dynasty_orstengard
	culture = west_divenori
	religion = southern_cult_of_castellos
	
	trait = race_human
	trait = curious
	trait = zealous
	trait = craven
	trait = ambitious
	trait = beauty_good_2
	disallow_random_traits = yes
	
	father = orstengard_0009
	mother = tefori_0007
	
	1008.8.5 = {
		birth = yes
	}
}


# Nirhaunes Dynasty

nirhaunes_0001 = {
	name = "Dalla"
	dynasty = dynasty_nirhaunes
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = gluttonous
	trait = generous
	trait = shy
	
	977.2.12 = {
		birth = yes
	}
	993.12.2 = {
		add_spouse = orstengard_0006
	}
}

nirhaunes_0002 = {
	name = "Haraldr"
	dynasty = dynasty_nirhaunes
	culture = dalric
	religion = skaldhyrric_faith
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = wrathful
	trait = diligent
	trait = reaver
	trait = desert_warrior
	trait = scarred
	trait = strong
	
	866.7.28 = {
		birth = yes
	}
	910.8.10 = {
		add_spouse = tefori_0008
	}
	939.9.6 = {
		death = yes
	}
}

nirhaunes_0003 = {
	name = "Jormar"
	dynasty = dynasty_nirhaunes
	culture = dalric
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_2
	trait = honest
	trait = wrathful
	trait = humble
	trait = reaver
	
	father = nirhaunes_0002
	mother = tefori_0008
	
	915.4.15 = {
		birth = yes
	}
	947.7.31 = {
		add_spouse = tefori_0009
	}
	972.1.31 = {
		death = yes
	}
}

nirhaunes_0004 = {
	name = "Baldur"
	dynasty = dynasty_nirhaunes
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = forgiving
	trait = humble
	trait = reaver
	
	father = nirhaunes_0003
	mother = tefori_0009
	
	948.10.21 = {
		birth = yes
	}
	971.8.25 = {
		add_spouse = tefori_0010
	}
	986.9.6 = {
		death = {
		death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

nirhaunes_0005 = {
	name = "Darres"
	dynasty = dynasty_nirhaunes
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = stubborn
	trait = vengeful
	trait = aggressive_attacker
	
	father = nirhaunes_0004
	mother = tefori_0010
	
	980.2.6 = {
		birth = yes
	}
	1009.1.8 = {
		add_spouse = luminares_0010
		set_relation_soulmate = character:luminares_0010
	}
}

nirhaunes_0006 = {
	name = "Araxia"
	dynasty = dynasty_nirhaunes
	culture = west_divenori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = curious
	
	father = nirhaunes_0005
	mother = luminares_0010
	
	1009.10.8 = {
		birth = yes
	}
}

nirhaunes_0007 = {
	name = "Baldur"
	dynasty = dynasty_nirhaunes
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = rowdy
	
	father = nirhaunes_0005
	mother = luminares_0010
	
	1014.1.7 = {
		birth = yes
	}
}

nirhaunes_0008 = {
	name = "TomE_"
	dynasty = dynasty_nirhaunes
	culture = west_divenori
	religion = tefori_damish
	
	trait = race_human
	trait = pensive
	
	father = nirhaunes_0005
	mother = luminares_0010
	
	1017.4.12 = {
		birth = yes
	}
}

nirhaunes_0009 = {
	name = "Marina"
	dynasty = dynasty_nirhaunes
	culture = west_divenori
	religion = tefori_damish
	female = yes

	trait = race_human
	
	father = nirhaunes_0005
	mother = luminares_0010
	
	1020.11.30 = {
		birth = yes
	}
}

#Mainland tefori

mircatares_0001 = {
	name = Federic
	dynasty = dynasty_mircatares
	culture = tefori
	religion = tefori_damish
	father = mircatares_0005

	trait = race_human
	trait = education_stewardship_2
	trait = greedy
	trait = diligent
	trait = callous
	trait = intellect_good_1
	trait = lifestyle_blademaster_2_history
	trait = irritable

	985.11.1 = {
		birth = yes
	}
	1010.1.1 = {
		effect = {
			set_relation_friend = { reason = friend_generic_history target = character:rivares_0003 }
		}
	}
	1013.6.23 = {
		add_spouse = rivares_0002
	}
}

rivares_0002 = {
	name = Iona
	dynasty = dynasty_rivares
	culture = west_divenori
	religion = tefori_damish
	female = yes
	father = rivares_0001

	trait = race_human
	trait = education_intrigue_2
	trait = deceitful
	trait = eccentric
	trait = gregarious
	trait = loyal
	
	989.6.26 = {
		birth = yes
	}
	1013.6.23 = {
		add_spouse = mircatares_0001
	}
}

mircatares_0002 = {
	name = Dosta_n
	dynasty = dynasty_mircatares
	culture = tefori
	religion = tefori_damish
	father = mircatares_0001
	mother = rivares_0002

	trait = race_human
	trait = pensive
	trait = intellect_good_1

	1015.5.24 = {
		birth = yes
	}
}

mircatares_0003 = {
	name = Lisbana
	dynasty = dynasty_mircatares
	culture = tefori
	religion = tefori_damish
	father = mircatares_0001
	mother = rivares_0002
	female = yes

	trait = race_human
	trait = curious

	1017.7.20 = {
		birth = yes
	}
}

mircatares_0004 = {
	name = Adelmar
	dynasty = dynasty_mircatares
	culture = tefori
	religion = tefori_damish
	father = mircatares_0005

	trait = race_human
	trait = education_diplomacy_3
	trait = humble
	trait = cynical
	trait = content
	trait = lifestyle_blademaster

	988.9.17 = {
		birth = yes
	}
}

mircatares_0005 = {
	name = Alonso
	dynasty = dynasty_mircatares
	culture = tefori
	religion = tefori_damish

	trait = race_human

	952.8.30 = {
		birth = yes
	}
}

rivares_0001 = {
	name = Jaborn
	dynasty = dynasty_rivares
	culture = west_divenori
	religion = tefori_damish

	trait = race_human

	944.3.12 = {
		birth = yes
	}
	1012.9.22 = {
		death = yes
	}
}

rivares_0003 = {
	name = Vilhame
	dynasty = dynasty_rivares
	culture = west_divenori
	religion = tefori_damish
	father = rivares_0001

	trait = race_human
	trait = education_martial_2
	trait = wrathful
	trait = generous
	trait = stubborn
	trait = forder
	trait = physique_good_1

	980.4.30 = {
		birth = yes
	}
	1013.2.11 = {
		add_spouse = mircatares_0006
	}
}

mircatares_0006 = {
	name = Ariana
	dynasty = dynasty_mircatares
	culture = tefori
	religion = tefori_damish
	female = yes
	father = mircatares_0005

	trait = race_human
	trait = chaste
	trait = shy
	trait = lazy

	991.3.4 = {
		birth = yes
	}
	1013.2.11 = {
		add_spouse = rivares_0003
	}
}

rivares_0004 = {
	name = Darres
	dynasty = dynasty_rivares
	culture = west_divenori
	religion = tefori_damish
	father = rivares_0003
	mother = mircatares_0006

	trait = race_human
	trait = rowdy

	1014.3.23 = {
		birth = yes
	}
}

seaborn_0001 = {
	name = Varilor
	dynasty = dynasty_seaborn
	culture = moon_elvish
	religion = elven_forebears

	trait = education_martial_1
	trait = diligent
	trait = trusting
	trait = content
	trait = intellect_good_1
	trait = forder
	trait = race_elf

	746.7.24 = {
		birth = yes
	}
	1014.2.25 = {
		add_spouse = seaborn_0002
	}
}

seaborn_0002 = {
	name = Nedin
	culture = businori
	religion = businori_cult
	female = yes

	trait = education_stewardship_2
	trait = lustful
	trait = ambitious
	trait = just
	trait = race_human

	987.9.14 = {
		birth = yes
	}
	1014.2.25 = {
		add_spouse = seaborn_0001
	}
}

seaborn_0003 = {
	name = Celiadora
	dynasty = dynasty_seaborn
	culture = businori
	religion = businori_cult
	female = yes
	father = seaborn_0001
	mother = seaborn_0002

	trait = curious
	trait = beauty_good_2
	trait = race_half_elf

	1016.6.12 = {
		birth = yes
	}
}

seaborn_0004 = {
	name = Tri_an
	dynasty = dynasty_seaborn
	culture = businori
	religion = businori_cult
	father = seaborn_0001
	mother = seaborn_0002

	trait = pensive
	trait = race_half_elf

	1018.8.22 = {
		birth = yes
	}
}

# Hortoselez Dynasty

hortoselez_0001 = {
	name = "Olayo"
	dynasty = dynasty_hortoselez
	culture = businori
	religion = businori_cult
	
	trait = race_human
	trait = education_intrigue_2
	trait = deceitful
	trait = humble
	trait = stubborn
	trait = loyal
	
	980.2.6 = {
		birth = yes
	}
}

# Taulez Dynasty
taulez_0001 = {
	name = "BreogA_n"
	dynasty = dynasty_taulez
	culture = businori
	religion = businori_cult
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = arrogant
	trait = stubborn
	
	997.4.9 = {
		birth = yes
	}
}

#Wives from unique dynasties

urzhana_0001 = {
	name = "Nairia"
	dynasty = dynasty_urzhana
	culture = bahari
	religion = jaherian_cults
	female = yes
	
	trait = race_human
	trait = education_learning_2
	trait = arrogant
	trait = gregarious
	trait = just
	
	880.7.29 = {
		birth = yes
	}
	897.6.10 = {
		add_spouse = taefdares_0001
	}
	928.2.23 = {
		death = yes
	}
}

azaraha_0001 = {
	name = "Arekis"
	dynasty = dynasty_azaraha
	culture = brasanni
	religion = jaherian_cults
	female = yes
	
	trait = race_human
	trait = education_stewardship_1
	trait = zealous
	trait = lazy
	trait = stubborn
	
	891.12.14 = {
		birth = yes
	}
	910.10.15 = {
		add_spouse = taefdares_0001
	}
	955.5.3 = {
		death = yes
	}
}

tefori_0001 = {
	name = "Isabela"
	# lowborn
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_intrigue_2
	trait = gluttonous
	trait = cynical
	trait = honest
	
	887.5.30 = {
		birth = yes
	}
	908.8.25 = {
		add_spouse = taefdares_0002
	}
	960.10.7 = {
		death = yes
	}
}


khasani_0001 = {
	name = "Nefori"
	dynasty = dynasty_khasani
	religion = mother_akasik
	culture = khasani
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = cynical
	trait = arrogant
	trait = gregarious
	
	951.11.29 = {
		birth = yes
	}
	972.9.21 = {
		add_spouse = taefdares_0007
	}
}


tefori_0002 = {
	name = "Revhild"
	# lowborn
	culture = black_castanorian
	religion = castanorian_pantheon
	female = yes
	
	trait = race_human
	trait = education_intrigue_3
	trait = deceitful
	trait = gregarious
	trait = fickle
	
	962.3.10 = {
		birth = yes
	}
	988.8.26 = {
		add_spouse = taefdares_0011
	}
}

waasheshi_0001 = {
	name = "Nesubane"
	dynasty = dynasty_waasheshi
	culture = shasoura
	religion = khetarchy
	female = yes
	
	trait = race_human
	trait = education_martial_2
	trait = ambitious
	trait = brave
	trait = impatient
	
	834.4.5 = {
		birth = yes
	}
	856.3.11 = {
		add_spouse = luminares_0001
	}
	893.1.2 = {
		death = yes
	}
}

tefori_0003 = {
	name = "Sibila"
	# lowborn
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_intrigue_2
	trait = greedy
	trait = calm
	trait = humble
	
	870.4.21 = {
		birth = yes
	}
	895.7.5 = {
		add_spouse = luminares_0002
	}
	930.10.4 = {
		death = yes
	}
}

tefori_0004 = {
	name = "Adriana"
	# lowborn
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = arrogant
	trait = just
	trait = stubborn
	
	905.6.3 = {
		birth = yes
	}
	928.3.16 = {
		add_spouse = luminares_0004
	}
	942.5.17 = {
		death = yes
	}
}

	
tefori_0005 = {
	name = "Amaral"
	# lowborn
	culture = ilatani
	religion = cult_of_the_lightfather
	female = yes
	
	trait = race_human
	trait = education_diplomacy_3
	trait = gregarious
	trait = honest
	trait = paranoid
	
	933.12.8 = {
		birth = yes
	}
	950.2.11 = {
		add_spouse = luminares_0005
	}
	1004.7.3 = {
		death = yes
	}
}

tefori_0006 = {
	name = "Marina"
	# lowborn
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = arrogant
	trait = just
	trait = stubborn
	
	966.9.10 = {
		birth = yes
	}
	987.7.21 = {
		add_spouse = luminares_0009
	}
	1015.2.23 = {
		death = yes
	}
}

tefori_0007 = {
	name = "Mioara"
	# lowborn
	culture = ourdi
	religion = southern_cult_of_castellos
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = honest
	trait = lustful
	trait = gregarious
	trait = beauty_good_3
	
	981.6.2 = {
		birth = yes
	}
	999.5.17 = {
		add_spouse = orstengard_0009
	}
}

tefori_0008 = {
	name = "Marina"
	# lowborn
	culture = tefori
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = arrogant
	trait = cynical
	trait = fickle
	
	894.3.28 = {
		birth = yes
	}
	910.8.10 = {
		add_spouse = nirhaunes_0002
	}
	970.2.16 = {
		death = yes
	}
}

tefori_0009 = {
	name = "Lydia"
	# lowborn
	culture = dalric
	religion = tefori_damish
	female = yes
	
	trait = race_human
	trait = education_learning_1
	trait = impatient
	trait = forgiving
	trait = humble
	
	930.5.25 = {
		birth = yes
	}
	947.7.31 = {
		add_spouse = nirhaunes_0003
	}
	988.10.4 = {
		death = yes
	}
}

tefori_0010 = {
	name = "Araxia"
	# lowborn
	culture = bahari
	religion = jaherian_cults
	female = yes
	
	trait = race_human
	trait = education_stewardship_3
	trait = honest
	trait = compassionate
	trait = greedy
	trait = shrewd
	
	945.7.6 = {
		birth = yes
	}
	971.8.25 = {
		add_spouse = nirhaunes_0004
	}
	999.12.13 = {
		death = yes
	}
}