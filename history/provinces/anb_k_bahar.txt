#k_bahar
##d_aqatbahar
###c_aqatbar
536 = {     #Azka-Triganas

    # Misc
    culture = bahari
    religion = jaherian_cults
	holding = castle_holding

    # History
}
2891 = {    #Aqatbar

    # Misc
    religion = jaherian_cults
    holding = city_holding

    # History

}
2892 = {    #Eduz-Ginakku

    # Misc
    religion = jaherian_cults
    holding = church_holding

    # History

}
2893 = {    #Shaztun-az-bar

    # Misc
    religion = jaherian_cults
    holding = city_holding

    # History
    effect = { add_province_modifier = marble_dwarvish_city }

}
2894 = {    #Pirubahar

    # Misc
    religion = jaherian_cults
    holding = none

    # History

}
2895 = {    #Tusar

    # Misc
    religion = jaherian_cults
    holding = none

    # History

}

###c_eduz_szel_ninezim
541 = {		#Eduz-szel-Ninezim

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = church_holding

    # History
}
2896 = {    #Arekost

    # Misc
    culture = bahari
    religion = rite_of_birsartan
    holding = city_holding

    # History

}
2897 = {

    # Misc
    culture = bahari
    religion = rite_of_birsartan
    holding = none

    # History

}

###c_nisabat
532 = {		#Nisabat

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2898 = {

    # Misc
    religion = rite_of_birsartan
    holding = church_holding

    # History

}
2899 = {

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}

###c_yesdbar
544 = {		#yesdbar

    # Misc
    culture = bahari
    religion = cult_of_the_eseral_mitellu
	holding = castle_holding

    # History
}
2934 = {

    # Misc
    religion = cult_of_the_eseral_mitellu
    holding = church_holding

    # History

}

###c_arzdilsu
537 = {		#Arzdilsu

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2931 = {

    # Misc
    religion = rite_of_birsartan
    holding = church_holding

    # History

}
2932 = {

    # Misc
    religion = rite_of_birsartan
    holding = city_holding

    # History

}
2933 = {

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}

##d_zagnabad
###c_vernat
530 = {		#Vernat

    # Misc
    culture = yametsesi
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2859 = {    #Kerusamsi

    # Misc
    religion = rite_of_birsartan
    holding = city_holding

    # History

}

###c_eduz_wez
531 = {		#Eduz-Wez

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
	holding = city_holding

    # History
}
2860 = {    #Eduz-Surellu

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
    holding = church_holding

    # History

}
2861 = {    #Pasah

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
    holding = none

    # History

}

###c_agselum
535 = {		#Agselum

    # Misc
    culture = bahari
    religion = cult_of_the_eseral_mitellu
	holding = castle_holding

    # History
}
2862 = {    #Arsabittu

    # Misc
    culture = bahari
    religion = cult_of_the_eseral_mitellu
    holding = city_holding

    # History

}
2863 = {    #Karhags

    # Misc
    culture = bahari
    religion = cult_of_the_eseral_mitellu
    holding = none

    # History

}

###c_bazibar
534 = {		#Bazibar

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2864 = {    #Basilan

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}

##d_deskumar
###c_deskumar
543 = {		#Deskumar

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2867 = {

    # Misc
    religion = rite_of_birsartan
    holding = city_holding

    # History

}
2868 = {

    # Misc
    religion = rite_of_birsartan
    holding = church_holding

    # History

}

###c_fajabahar
542 = {		#Fajabahar

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
	holding = castle_holding

    # History
}
2869 = {

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
    holding = city_holding

    # History

}
2870 = {

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
    holding = none

    # History

}

##d_bahar_szel_uak
###c_akal_uak
550 = {		#Akal Uak

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2871 = {

    # Misc
    religion = rite_of_birsartan
    holding = city_holding

    # History

}
2872 = {

    # Misc
    religion = rite_of_birsartan
    holding = church_holding

    # History

}
2873 = {

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}
2874 = {

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}

###c_tremo_enkost
547 = {		#Tremo'enkost

    # Misc
    culture = sun_elvish
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2875 = {

    # Misc
    religion = rite_of_birsartan
    holding = church_holding

    # History

}
2876 = {

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}
2877 = {

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}

###c_habqez
548 = {		#Habqez

    # Misc
    culture = bahari
    religion = cult_of_kuza
	holding = castle_holding

    # History
}
2878 = {

    # Misc
    religion = cult_of_kuza
    holding = city_holding

    # History

}
2879 = {

    # Misc
    holding = none

    # History

}

###c_forramaz
545 = {		#Forramaz

    # Misc
    culture = bahari
    religion = rite_of_birsartan
	holding = castle_holding

    # History
}
2880 = {

    # Misc
    religion = rite_of_birsartan
    holding = none

    # History

}

##d_azka_evran
###c_azka_evran
538 = {		#Azka-Evran

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}
2881 = {

    # Misc
    religion = sundancer_paragons
    holding = city_holding

    # History

}
2883 = {

    # Misc
    religion = sundancer_paragons
    holding = none

    # History

}
2884 = {

    # Misc
    religion = sundancer_paragons
    holding = none

    # History

}
