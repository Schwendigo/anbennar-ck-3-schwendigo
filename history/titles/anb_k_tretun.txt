k_tretun = {
	1000.1.1 = { change_development_level = 8 }
	884.5.8 = {
		holder = 0
	}
	880.10.13 = {
		holder = achanis_0001
	}
	855.3.17 = {
		holder = tretunis_0017
	}
	830.10.22 = {
		holder = tretunis_0016
	}
	803.5.2 = {
		holder = tretunis_0014
	}
	752.4.20 = { 
		holder = tretunis_0013
	}
	716.9.24 = {
		holder = tretunis_0012
	}
	680.3.3 = {
		holder = tretunis_0011
	}
	673.2.19 = {
		holder = tretunis_0010
	}
	668.12.10 = {
		holder = tretunis_0009
	}
	605.9.14 = {
		holder = tretunis_0008
	}
	592.2.7 = {
		holder = tretunis_0006
	}
	572.12.7 = {
		holder = tretunis_0005
	}
	532.3.12 = {
		holder = tretunis_0003
	}
	504.12.7 = {
		holder = tretunis_0002
	}
	474.1.1 = {
		holder = tretunis_0001
	}
}

d_tretun = {
	994.7.11 = {
		holder = nahilnis_0003
	}
	982.11.8 = {
		holder = nahilnis_0002
	}
	956.12.4 = {
		holder = nahilnis_0001
	}
	928.11.14 = {
		holder = bladeborn_0003
	}
	894.6.25 = {
		holder = bladeborn_0001
	}
}

c_tretun = {
	1000.1.1 = { change_development_level = 9 }
	994.7.11 = {
		holder = nahilnis_0003
	}
	982.11.8 = {
		holder = nahilnis_0002
	}
	956.12.4 = {
		holder = nahilnis_0001
	}
	928.11.14 = {
		holder = bladeborn_0003
	}
	894.6.25 = {
		holder = bladeborn_0001
	}
	885.11.30 = {
		holder = pearlman_0002 # Haakon I Henriksson
	}
	880.10.13 = {
		holder = pearlman_0001
	}
	855.3.17 = {
		holder = tretunis_0017
	}
	830.10.22 = {
		holder = tretunis_0016
	}
	803.5.2 = {
		holder = tretunis_0014
	}
	752.4.20 = { 
		holder = tretunis_0013
	}
	716.9.24 = {
		holder = tretunis_0012
	}
	680.3.3 = {
		holder = tretunis_0011
	}
	673.2.19 = {
		holder = tretunis_0010
	}
	668.12.10 = {
		holder = tretunis_0009
	}
	605.9.14 = {
		holder = tretunis_0008
	}
	592.2.7 = {
		holder = tretunis_0006
	}
	572.12.7 = {
		holder = tretunis_0005
	}
	532.3.12 = {
		holder = tretunis_0003
	}
	504.12.7 = {
		holder = tretunis_0002
	}
	474.1.1 = {
		holder = tretunis_0001
	}
}

c_lanpool = {
	1000.1.1 = { change_development_level = 8 }
	1012.3.1 = {
		holder = lanpool_0005
	}
	981.10.7 = {
		holder = lanpool_0003
	}
	964.7.31 = {
		holder = lanpool_0002
	}
	928.11.14 = {
		holder = lanpool_0001
		liege = d_tretun
	}
	894.6.25 = {
		holder = bladeborn_0001
	}
	880.10.13 = {
		holder = 0
	}
	855.3.17 = {
		holder = tretunis_0017
	}
	830.10.22 = {
		holder = tretunis_0016
	}
	803.5.2 = {
		holder = tretunis_0014
	}
	752.4.20 = { 
		holder = tretunis_0013
	}
	716.9.24 = {
		holder = tretunis_0012
	}
	680.3.3 = {
		holder = tretunis_0011
	}
	673.2.19 = {
		holder = tretunis_0010
	}
	668.12.10 = {
		holder = tretunis_0009
	}
	605.9.14 = {
		holder = tretunis_0008
	}
	592.2.7 = {
		holder = tretunis_0006
	}
	572.12.7 = {
		holder = tretunis_0005
	}
	532.3.12 = {
		holder = tretunis_0003
	}
	504.12.7 = {
		holder = tretunis_0002
	}
	474.1.1 = {
		holder = tretunis_0001
	}
}

d_roilsard = {
	1000.1.1 = { change_development_level = 11 }
	872.1.1 = {
		holder = roilsardis_0001
	}
	906.7.19 = {
		holder = roilsardis_0002
	}
	915.10.11 = {
		holder = roilsardis_0005
	}
	951.12.1 = {
		holder = roilsardis_0008
	}
	979.5.29 = {
		holder = 0
	}
}

c_roilsard = {
	1001.1.1 = { change_development_level = 12 }
	979.5.29 = {
		holder = roilsardis_0016
	}
	982.8.14 = {
		holder = 504
	}
	1005.4.30 = {
		holder = 6	#Gregoire Roilsard
	}
}

c_saloren = {
	998.3.23 = {
		holder = 501	#Artur síl Saloren
	}
}

c_vivinmar = {
	979.5.29 = {
		holder = roilsardis_0010
	}
	989.4.15 = {
		holder = 508 #Marcel II síl Vivin
	}
	1001.1.12 = {
		holder = 4	#Petrus sil Vivin
	}
}


c_loopuis = {
	979.5.29 = {
		holder = roilsardis_0013
	}
	993.6.12 = {
		holder = 5	#Caylen sil na Loop
	}
}