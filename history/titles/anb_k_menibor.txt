﻿k_menibor = {
	1000.1.1 = { change_development_level = 10 }
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0004	#Maurisio sil Menibor
	}
}

d_menibor_loop = {
	1000.1.1 = { change_development_level = 10 }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0004	#Maurisio sil Menibor
	}
}

c_menibor = {
	1000.1.1 = { change_development_level = 17 }
}

c_napesbay = {
	1000.1.1 = { change_development_level = 9 }
}

d_galeinn = {
	1000.1.1 = { change_development_level = 9 }
}

c_galeinn = {
	1020.1.1 = {
		holder = laudaris_0001
		liege = k_menibor
	}
}

c_walterton = {
	1000.1.1 = { change_development_level = 9 }
	980.2.10 = {
		holder = walterid_0001
		liege = k_menibor
	}
}

c_bellacaire = {
	1000.1.1 = { change_development_level = 16 }
	1021.2.16 = {
		holder = vernid_0008
		liege = k_menibor
	}
}

c_whistlepoint = {
	1021.2.16 = {
		holder = vernid_0008
		liege = k_menibor
	}
}

c_idhaine = {
	1000.1.1 = { change_development_level = 12 }
	1020.1.1 = {
		holder = treun_0001
		liege = k_menibor
	}
}