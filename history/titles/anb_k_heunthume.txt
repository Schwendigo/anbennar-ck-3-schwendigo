k_heunthume = {
	1018.3.5 = { holder = heunthulyra001 }
}

c_thissilen = {
	1000.1.1 = { change_development_level = 13 }
}

c_nasru_ean = {
	1000.1.1 = { change_development_level = 10 }
}

c_rianelen = {
	1000.1.1 = { change_development_level = 12 }
}

c_daqelum = {
	1000.1.1 = { change_development_level = 10 }
}

c_tounafira = {
	1000.1.1 = { change_development_level = 15 }
}

c_anbennar_2995 = {
	1000.1.1 = { change_development_level = 12 }
}

c_hisarytor = {
	1000.1.1 = { change_development_level = 11 }
}

c_jarmevain = {
	1000.1.1 = { change_development_level = 13 }
}

c_setufiraor = {
	1000.1.1 = { change_development_level = 11 }
}

c_haqharias = {
	1000.1.1 = { change_development_level = 10 }
}

c_hytiranyalen = {
	1000.1.1 = { change_development_level = 13 }
}

c_lisinyalen = {
	1018.3.5 = { holder = heunthulyra001 }
	1000.1.1 = { change_development_level = 12 }
}

c_heunthume = {
	1018.3.5 = { holder = heunthulyra001 }
	1000.1.1 = { change_development_level = 19 }
}

c_ayarallen = {
	1000.1.1 = { change_development_level = 12 }
}

c_arnilqan = {
	1000.1.1 = { change_development_level = 11 }
}