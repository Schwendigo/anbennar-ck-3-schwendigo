﻿# We can use these modifiers for adding specific beards and beards to historical and vanity characters

beards_scripted_characters = {

	usage = game
	selection_behavior = weighted_random

	male_beard_western_01 = { # Standard full beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.04 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:79 # Garrec Ebonfrost
				this = character:79 # Garrec Ebonfrost
			}
			modifier = {
				add = 200
				exists = character:106 # Kolvan Karnid
				this = character:106 # Kolvan Karnid
			}
			modifier = {
				add = 200
				exists = character:123 # Devan
				this = character:123 # Devan
			}
			modifier = {
				add = 200
				exists = character:20002 # Riualtr Aubergentis
				this = character:20002 # Riualtr Aubergentis
			}
		}
	}

	male_beard_western_02 = { # Standard goatee
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.07 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:120 # Vasile síl Fiachlar
				this = character:120 # Vasile síl Fiachlar
			}
			modifier = {
				add = 200
				exists = character:121 # Henric Verteben
				this = character:121 # Henric Verteben
			}
			modifier = {
				add = 200
				exists = character:554 # Beltram Ventis
				this = character:554 # Beltram Ventis
			}
		}
	}

	male_beard_western_03 = { # Short beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.11 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:43 # Eustace síl Uelaire
				this = character:43 # Eustace síl Uelaire
			}
			modifier = {
				add = 200
				exists = character:169 # Diaco Kobali
				this = character:169 # Diaco Kobali
			}
			modifier = {
				add = 200
				exists = character:20000 # Talan Aubergentis
				this = character:20000 # Talan Aubergentis
			}
		}
	}

	male_beard_western_04 = { # Short goatee
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.14 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
		
	}

	male_beard_western_05 = { # Mustache
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.17 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:583 # Roen Arthelis
				this = character:583 # Roen Arthelis
			}
		}
	}

	male_beard_western_06 = { # Big bushy beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.2 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:harascilde_0026 # Caradan "the Shield of the West"
				this = character:harascilde_0026 # Caradan "the Shield of the West"
			}
		}
	}

	male_beard_western_07 = { # Chin strap
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.23 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:76 # Martin Farran
				this = character:76 # Martin Farran
			}
		}
	}

	male_beard_western_08 = { # Big with fancy mustache
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.27 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_western_09 = { # Pointy beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.30 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:62 # Rylen Adshaw
				this = character:62 # Rylen Adshaw
			}
		}
	}

	male_beard_western_10 = { # Chin goatee
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.33 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_mena_01 = { # Very curly medium length full beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.35 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_mena_02 = { # Big with small mustache
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.39 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_mena_03 = { # Big chin strap
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.42 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_mena_04 = { # Short stylish
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.45 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:dameris_0014 # Crege Dameris
				this = character:dameris_0014 # Crege Dameris
			}
		}
	}

	male_beard_northern_01 = { # Big wide
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.48 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:gawe_0027 # Alenn Gawe 'the Elder'
				this = character:gawe_0027 # Alenn Gawe 'the Elder'
			}
		}
	}

	male_beard_northern_02 = { # Single braid
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.52 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:41 # Bolgrun Redstone
				this = character:41 # Bolgrun Redstone
			}
		}
	}

	male_beard_northern_03 = { # Beaded beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.54 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:94 # Urvik Ebonfrost
				this = character:94 # Urvik Ebonfrost
			}
		}
	}

	male_beard_steppe_01 = { # long at chin with mustache
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.58 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0

		}
	}

	male_beard_steppe_02 = { # low rough 
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.61 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:iochand_0001 # Carwick Iochand
				this = character:iochand_0001 # Carwick Iochand
			}
		}
	}

	male_beard_sub_saharan_01 = { # Full african beard medium
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.64 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:96 # Daynan Sarfort
				this = character:96 # Daynan Sarfort
			}
		}
	}

	male_beard_sub_saharan_02 = { # Stylish african goatee
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.67 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_sub_saharan_03 = { # Full african beard long
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.70 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_indian_01 = { # Handlebar mustache
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.74 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:vernid_0002 # Rocair Vernid
				this = character:vernid_0002 # Rocair Vernid
			}
			modifier = {
				add = 200
				exists = character:98 # Toman 'Slothsword'
				this = character:98 # Toman 'Slothsword'
			}
			modifier = {
				add = 200
				exists = character:rewantis_0025 # Jaspar Rewantis
				this = character:rewantis_0025 # Jaspar Rewantis
			}
		}
	}

	male_beard_indian_02 = { # Indian pointy
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.77 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_indian_03 = { # Very curly medium length full beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.80 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_rtt_01 = { # Wizard beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.0 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_fp1_01 = { # Twin braids with metal rings
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.84 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_fp1_02 = { # Short low beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.88 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_fp1_03 = { # Wavy pointy beard
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.90 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
		}
	}

	male_beard_fp1_04 = { # Huge braid
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.94 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:300002 #Skali Sidaett
				this = character:300002 #Skali Sidaett
			}
		}
	}

	male_beard_fp1_05 = { # Long narrow wavy
		dna_modifiers = {
			accessory = {
				mode = add
				gene = beards
				template = scripted_character_beards_01
				value = 0.97 # For the randomness to work correctly
			}
		}   
		weight = {
			base = 0
			modifier = {
				add = 200
				exists = character:fuglborg_0001 #Einar
				this = character:fuglborg_0001 #Einar
			}
		}
	}
}