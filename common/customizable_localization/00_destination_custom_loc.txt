﻿# Anbennar: commented vanilla stuff

# Language Descriptor
DestinationInspiredLanguageDescriptor = {
	type = province

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_anglic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorAnglic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_aramaic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorAramaic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_baltic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorBaltic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_basque }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorBasque
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_berber }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorBerber
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_brythonic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorBrythonic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_burmese }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorBurmese
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_central_germanic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorCentralGermanic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_chadic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorChadic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_chinese }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorChinese
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_cushitic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorCushitic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_sabaki }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSabaki
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_dutch }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorDutch
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_east_slavic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorEastSlavic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_ethiopic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorEthiopic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_finnic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorFinnic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_frankish }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorFrankish
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_french }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorFrench
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_goidelic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorGoidelic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_gur }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorGur
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_high_german }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorHighGerman
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_iberian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorIberian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_sauraseni }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSauraseni
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_marathi }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorMarathi
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_magadhan }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorMagadhan
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_pahari }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorPahari
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_rajasthani }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorRajasthani
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_vrachada }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorVrachada
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_sinhalese }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSinhalese
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_kashmiri }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorKashmiri
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_tamil }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorTamil
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_telugu }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorTelugu
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_kannada }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorKannada
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_north_dravidian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorNorthDravidian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_iranian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorIranian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_israelite }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorIsraelite
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_latin }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorLatin
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_kru }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorKru
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_kwa }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorKwa
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_magyar }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorMagyar
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_manding }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorManding
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_mongolic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorMongolic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_norse }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorNorse
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_oghur }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorOghur
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_omotic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorOmotic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_qiangic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorQiangic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_sami }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSami
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_saxon }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSaxon
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_senegambian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSenegambian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_soninke }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSoninke
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_south_slavic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSouthSlavic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_slavonic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSlavonic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_sudanic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSudanic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_tibetan }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorTibetan
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_tubu }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorTubu
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_turkic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorTurkic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_ugro_permian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorUgroPermian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_volga_finnic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorVolgaFinnic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_yoruba }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorYoruba
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_armenian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorArmenian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_greek }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorGreek
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_scythian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorScythian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_georgian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorGeorgian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_occitano_romance }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorOccitanoRomance
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_lechitic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorLechitic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_czech_slovak }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorCzechSlovak
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_tocharian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorTocharian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_tungusic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorTungusic
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_sardinian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorSardinian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_wallachian }
		# }
		# fallback = yes
		# localization_key = DestinationInspiredLanguageDescriptorWallachian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_italian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorItalian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_ayneha }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorAyneha
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_egyptian }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorEgyptian
	# }

	# text = { # Anbennar
		# trigger = {
			# culture = { has_cultural_pillar = language_hunnic }
		# }
		# localization_key = DestinationInspiredLanguageDescriptorHunnic
	# }
	
	# Anbennar TODO: add all our languages
	
	text = {
		trigger = {
			OR = { # this could be a scripted trigger
				culture = { has_cultural_pillar = language_common }
				culture = { has_cultural_pillar = language_damerian_common }
				culture = { has_cultural_pillar = language_alenic_common }
				culture = { has_cultural_pillar = language_small_common }
				culture = { has_cultural_pillar = language_lencori_common }
				culture = { has_cultural_pillar = language_vernid_common }
				culture = { has_cultural_pillar = language_milcori_common }
				culture = { has_cultural_pillar = language_businori_common }
				culture = { has_cultural_pillar = language_reachman_common }
				culture = { has_cultural_pillar = language_castanorian_common }
				culture = { has_cultural_pillar = language_rohibonic_common }
				culture = { has_cultural_pillar = language_borders_common }
			}
		}
		localization_key = DestinationInspiredLanguageDescriptorCommon
	}
	
	### ADD REST HERE ###
	
	text = { # Fallback
		fallback = yes
		localization_key = DestinationInspiredLanguageDescriptorCommon
	}
}
