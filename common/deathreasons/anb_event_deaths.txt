﻿death_killed_war_of_sorcerer_king = {
	public_knowledge = yes
	icon = "death_murder.dds"
}

death_battle_of_balmire = {
	public_knowledge = yes
	icon = "death_murder.dds"
}

death_battle_of_morban_flats = {
	public_knowledge = yes
	icon = "death_murder.dds"
}

death_battle_of_trialmount = {
	public_knowledge = yes
	icon = "death_murder.dds"
}

death_battle_of_red_reach = {
	public_knowledge = yes
	icon = "death_murder.dds"
}

death_butchers_field_massacre = {
	public_knowledge = yes
	icon = "death_murder.dds"
}

death_damestear_meteorite = {
	public_knowledge = yes
	icon = "death_natural.dds"
}

death_retirement = {
	public_knowledge = yes
	icon = "death_unknown.dds"
}

death_necromancer_experiments = {
	public_knowledge = yes
	icon = "death_murder.dds"
}