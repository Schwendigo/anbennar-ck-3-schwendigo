﻿doctrine_infernalist = {
	group = "main_group"
	is_available_on_create = {
		religion_tag = infernal_court_religion
	}
	
	doctrine_hidden_infernalist = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_hidden_infernalist }
				multiply = 0
			}
		}
		character_modifier = {
			piety_level_impact_mult = -0.25 # Favour of devils is fickle
		}
		parameters = {
			sanctioned_false_conversion = yes
			pluralism_fundamentalist_vulnerable_to_heresy = yes
		}
	}

	doctrine_open_infernalist = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_open_infernalist }
				multiply = 0
			}
		}
		character_modifier = {
			piety_level_impact_mult = -0.1 # Favour of devils is slightly less fickle
			dread_gain_mult = 0.25
		}
		parameters = {
			pluralism_fundamentalist_vulnerable_to_heresy = yes
		}
	}
}
